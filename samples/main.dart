#import('../MockDart/MockDart.dart');

#source('IRandom.dart');
#source('Random.dart');

void main() {

  print("Normal mocking. ");
  var mr = new MockObject();
  mr.stub('something').shouldReturn('This is nice');
  mr.something();
  print('');
  
  print("How about returning functions. ");
  mr = new MockObject();
  mr.stub('something').shouldReturn(() => print ('This is just awesome'));
  mr.something("abcd", 1234)();
  print('');
  
  print("How about some arguments in your method? ");
  mr = new MockObject();
  mr.stub('something').shouldReturn('This is better');
  mr.something("abcd", 1234);
  print('');
  
  print("Can I mock based on input arguments? ");
  mr = new MockObject();
  mr.stub('something').forArguments(["abcd", 1234]).shouldReturn(() => print ('Yeah !!!'));
  mr.something("abcd", 1234)();
  print('');
  
  print('Can I mock just some of my arguments?');
  mr = new MockObject();
  mr.stub('something').forArguments(["abcd", Args.any()]).shouldReturn(() => print ('Sure !!!'));
  mr.something("abcd", 1234)();
  print('');
  
  print('Can I mock all my arguments in a different way?');
  mr = new MockObject();
  mr.stub('something').forArguments([Args.any(), Args.any()]).shouldReturn(() => print ('Definitely !!!'));
  mr.something("abcd", 12354)();
  print('');
  
  print('Record & Play');
  mr = new MockObject();
  mr.stub('something').recordAndPlay(["Hello", "World", "Dart", "Mocks"]);
  print(mr.something());
  print(mr.something());
  print(mr.something());
  print(mr.something());
  print(mr.something());
  print('');  
  

  print ('*******************************************************************');
  print('Whats next? ');
  print ('*******************************************************************');
  print('Semi mock objects');
  print('Asserting on number of calls, returning first results, then second results etc');
  print ('*******************************************************************');
  
}