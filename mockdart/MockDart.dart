#library('MockDart');

class MockObject {
  List<FunctionDefinition> mocks;
  
  FunctionDefinition currentFunction;
  
  MockObject() : mocks = new List<FunctionDefinition>(){}
  
  bool IsArgumentsAreMatch(currentArgs, args) {
    if (args.length == 0) return true;
    if(currentArgs.length != args.length) return false;
    bool isMatching = true;
    for(var idx=0;idx<currentArgs.length;idx++) {
      isMatching = isMatching && ((currentArgs[idx] == args[idx]) || args[idx] == Args.any()) ;
    }
    return isMatching;
  }
  
  Dynamic figureReturnValue(String functionName, FunctionDefinition functionDef) {
      if(functionDef.name == functionName) {
        return functionDef.returnValue;
      }
      return null;
  }
  
  noSuchMethod(String functionName, List args) {
    for(var idx = 0;idx<mocks.length;idx++) {
      if(mocks[idx].name == functionName) {
        if(IsArgumentsAreMatch(args, mocks[idx].arguments)) {
          var retVal = mocks[idx].returnValue[0];
          if(mocks[idx].returnValue.length > 1) mocks[idx].returnValue.removeRange(0, 1);
          return retVal;
        }
      }
    }
    throw "This method is not implemented";
  }
  
  MockObject stub(String methodName) {
    var definition = new FunctionDefinition(methodName);
    mocks.add(definition);
    this.currentFunction = definition; 
    return this;
  }
  
  MockObject forArguments(List<Dynamic> arguments) {
    this.currentFunction.arguments = arguments;
    return this;
  }
  
  MockObject shouldReturn(Dynamic returnValue) {
    this.currentFunction.returnValue.add(returnValue);
    return this;
  }
  
  MockObject recordAndPlay(List<Dynamic> returnValue) {
    this.currentFunction.returnValue.addAll(returnValue);
    return this;
  }
}

class FunctionDefinition {
   String name;
   List<Dynamic> arguments;
   List<Dynamic> returnValue;
   
   FunctionDefinition(String methodName) {
     name = methodName;
     arguments = [];
     returnValue = [];
   }
}

class Args {
  static Any privateAny;
  
  static Any any() {
    if(privateAny != null) return privateAny;
    privateAny = new Any(); 
    return privateAny;
  }
}

class Any {
  bool isAny() {
    return true;
  }
}